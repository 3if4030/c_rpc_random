/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "random.h"

#include <stdio.h>
#include <stdlib.h>

const char * host = "localhost";

void
random_bounds(int min, int max)
{
	CLIENT *clnt;
	void  *result_1;
	bounds  set_bounds_1_arg;
	set_bounds_1_arg.min = min;
	set_bounds_1_arg.max = max;
	clnt = clnt_create(host, RANDOM, VERSION_UN, "udp");
	if (clnt == NULL) {
		clnt_pcreateerror(host);
		exit(1);
	}
	result_1 = set_bounds_1(&set_bounds_1_arg, clnt);
	if (result_1 == NULL) {
		clnt_perror(clnt, "call failed:");
	}
	clnt_destroy( clnt );
}


void
random_get()
{
	CLIENT *clnt;
	int  *result_2;
	char*  next_random_1_arg;
	clnt = clnt_create(host, RANDOM, VERSION_UN, "udp");
	if (clnt == NULL) {
		clnt_pcreateerror(host);
		exit(1);
	}
	for (int i = 0; i < 10; ++i) {
    	result_2 = next_random_1((void*)&next_random_1_arg, clnt);
	    if (result_2 == NULL) {
		    clnt_perror(clnt, "call failed:");
	    }
	    printf("%d ", *result_2);
	}
	printf("\n");
	clnt_destroy( clnt );
}

int
main( int argc, char* argv[] )
{
	if(argc > 2) random_bounds(atoi(argv[1]), atoi(argv[2]));
	else random_get();
}


.PHONY: all clean

PROGS = random_server random_client

GENERATED_SRC = random_client.c random_clnt.c random_server.c random_svc.c random_xdr.c

OBJS = $(GENERATED_SRC:%.c=%.o)

CPPFLAGS += -DDEBUG

all: $(PROGS)

random_server: random_server.o random_svc.o random_xdr.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

random_client: random_client.o random_clnt.o random_xdr.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

$(GENERATED_SRC): random.x
	rpcgen -a -C random.x
	mv random_server.c random_server_orig.c
	cp random_server_ok.c random_server.c
	mv random_client.c random_client_orig.c
	cp random_client_ok.c random_client.c

%.o: %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) -o $@ $<

clean:
	rm -f $(PROGS) $(OBJS) $(GENERATED_SRC) random.h Makefile.random
